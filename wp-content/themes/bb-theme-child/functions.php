<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
if ( current_user_can( 'manage_options' ) ) {
	add_filter( 'show_admin_bar', '__return_true' );
}
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');





// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

/**
 * Dequeue the jQuery UI styles.
 *
 * Hooked to the wp_print_styles action, with a late priority (100),
 * so that it is after the style was enqueued.
 */
 function remove_pagelist_css() {
    wp_dequeue_style( 'page-list-style' );
 }
 add_action( 'wp_print_styles', 'remove_pagelist_css', 100 );

 wp_clear_scheduled_hook( 'my_schedule_hook' );

 //add method to register event to WordPress init
add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
   
}
add_filter( 'auto_update_plugin', '__return_false' );

add_action( '404_redirection_301_log_cronjob', 'custom_404_redirect_hook_CA' ); 

// custom 301 redirects from  404 logs table
function custom_404_redirect_hook_CA(){
    global $wpdb;    
    write_log('in function');

    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix.'redirection_groups';

     $data_404 = $wpdb->get_results( "SELECT * FROM $table_name" );
     $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
     $redirect_group =  $datum[0]->id;  

    if ($data_404)
    {      
      foreach ($data_404 as $row_404) 
       {            

        if (strpos($row_404->url,'carpet') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {
            write_log($row_404->url);      
            
           $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

           
            $destination_url_carpet = '/flooring/carpet/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_carpet,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            write_log( 'carpet 301 added ');
         }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');


         }

        }
        else if (strpos($row_404->url,'hardwood') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_hardwood = '/flooring/hardwood/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_hardwood,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }

            write_log( 'hardwood 301 added ');

        }
        else if (strpos($row_404->url,'laminate') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_laminate = '/flooring/laminate/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_laminate,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'laminate 301 added ');


        }
        else if ((strpos($row_404->url,'luxury-vinyl') !== false || strpos($row_404->url,'vinyl') !== false) && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);  

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
             $destination_url_lvt = '/flooring/vinyl/products/';
            
            
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_lvt,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }

            write_log( 'luxury-vinyl 301 added ');
        }
        else if (strpos($row_404->url,'tile') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

                $destination_url_tile = '/flooring/tile/products/';
                $match_url = rtrim($row_404->url, '/');

                $data = array("url" => $row_404->url,
                "match_url" => $match_url,
                "match_data" => "",
                "action_code" => "301",
                "action_type" => "url",
                "action_data" => $destination_url_tile,
                "match_type" => "url",
                "title" => "",
                "regex" => "true",
                "group_id" => $redirect_group,
                "position" => "1",
                "last_access" => current_time( 'mysql' ),
                "status" => "enabled");

                $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');
                $wpdb->insert($table_redirect,$data,$format);
                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }else{
                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }
            write_log( 'tile 301 added ');
        }
       }  
    }
}



